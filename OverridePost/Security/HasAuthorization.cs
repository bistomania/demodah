﻿using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace OverridePost.Security
{
    public class HasAuthorization : AuthorizeAttribute
    {
        public override void OnAuthorization(AuthorizationContext filterContext)
        {
            //filterContext.HttpContext.Request.Browser

            //Recupera o cookie 
            //HttpCookie perfilUsuario = filterContext.HttpContext.Request.Cookies["NomeCookie"];

            base.OnAuthorization(filterContext);

            if (filterContext.Result is HttpUnauthorizedResult)
            {
                filterContext.HttpContext.Response.Redirect("~/Home/Login");
            }
        }


        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                HttpCookie perfilUsuario = filterContext.HttpContext.Request.Cookies["perfilUsuarioCookie"];
                string usuarioAdm = "";

                if (perfilUsuario != null)
                {
                    usuarioAdm = perfilUsuario.Value;
                }

                if (!usuarioAdm.ToLower().Contains(base.Roles.ToLower()))
                {
                    filterContext.Result = new RedirectToRouteResult(new
                  RouteValueDictionary(new { controller = "Error", action = "AccessDenied" }));
                }
            }
            else
            {
                filterContext.HttpContext.Response.Redirect("~/Home/Login");

            }
        }
    }
}
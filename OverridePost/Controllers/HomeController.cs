﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace OverridePost.Controllers
{
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {

            //Seta o cookie
            //FormsAuthentication.SetAuthCookie("Login","lembrar senha (true/false");

            //Salva o perfil no cookie
            //HttpCookie perfilUsuarioCookie = new HttpCookie("perfilUsuarioCookie")
            //{
            //    Value = "Perfil",
            //    Expires = DateTime.Now.AddYears(50)
            //};
            //Response.Cookies.Add(perfilUsuarioCookie);

            //Salva o id no cookie
            //HttpCookie userCookie = new HttpCookie("UsuarioId")
            //{
            //    Value = "Usuario ID",
            //    Expires = DateTime.Now.AddYears(50)
            //};
            //Response.Cookies.Add(userCookie);
            
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}
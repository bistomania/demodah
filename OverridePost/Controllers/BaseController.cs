﻿using OverridePost.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OverridePost.Controllers
{
    [HasAuthorization]
    public class BaseController : Controller
    {
    }
}